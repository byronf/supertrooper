<cfcomponent hint="I replace all instances of 'now' with 'meow' " output="false">

	<cffunction name="preRender" access="public" returntype="void" hint="Executes before event data is rendered" output="false" >
		<cfargument name="event">
		<cfargument name="interceptData">
		
		<cfscript>
		// Just modifying Brad Woods, JavaScript-Defer / JavascriptDefer.cfc, with something fun (I think)	
		
		var local = {};
		// Regex to match script tags.
		local.regex = "<[\s\/]*script\b[^>]*>[^>]*<\/script>";

		local.result = reFindNoCase(local.regex,interceptData.renderedContent,1,true);

		// Replace all instances of 'now' and 'Now' with the appropriate 'Moew/moew'
		interceptData.renderedContent = Replace(interceptData.renderedContent, "Now", "Meow", "all");
		interceptData.renderedContent = Replace(interceptData.renderedContent, "now", "meow", "all");

		</cfscript>
		
	</cffunction>
		
</cfcomponent>